
import { BuildURLWithParams } from '../lib/api.helpers'
import { GetBillingApiUrl } from '../lib/const.helpers'
import { _getJSON, _patchJSON, _postJSON, _putJSON } from './api.service'

const serviceSubscriptionUrl = () => `${GetBillingApiUrl()}/subscriptions`
const serviceSubscriptionUrlPayment = () => `${GetBillingApiUrl()}/payments`
const serviceCustomerUrl = () => `${GetBillingApiUrl()}/customers`
const servicePricesUrl = () => `${GetBillingApiUrl()}/prices`

export const GetPrices = async (token) => {
  return _getJSON(servicePricesUrl(), token)
}

export const GetSubscriptions = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}`, params)
  return _getJSON(url, token)
}

export const GetSubscription = async (token, id, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}/${id}`, params)
  return _getJSON(url, token)
}

export const UpdateSubscriptions = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceSubscriptionUrl()}/${id}`, token, body)
}

export const CreateSubscriptions = async (token, body = {}) => {
  return _postJSON(`${serviceSubscriptionUrl()}`, token, body)
}

export const RefreshSubscription = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}/${id}/refresh`, params)
  return _putJSON(url, token)
}

export const GetCustomer = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceCustomerUrl()}/${id}`, params)
  return _getJSON(url)
}

export const GetCustomers = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceCustomerUrl()}`, params)
  return _getJSON(url, token)
}

export const UpdateCustomer = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceCustomerUrl()}/${id}`, token, body)
}

export const CreateCustomer = async (token, id = '', body = {}) => {
  return _postJSON(`${serviceCustomerUrl()}`, token, body)
}

export const GetPayment = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}/${id}`, params)
  return _getJSON(url, token)
}

export const RefreshPayment = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}/${id}/refresh`, params)
  return _putJSON(url, token)
}

export const GetPayments = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}`, params)
  return _getJSON(url, token)
}

export const UpdatePayment = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceSubscriptionUrlPayment()}/${id}`, token, body)
}

export const CreatePayment = async (token, id = '', body = {}) => {
  return _postJSON(`${serviceSubscriptionUrlPayment()}`, token, body)
}
