/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import DeviceDeleteButton from '../../molecules/DeviceDeleteButton/DeviceDeleteButton'
import DevicePublishButton from '../../molecules/DevicePublishButton/DevicePublishButton'
import RelativeTime from '../../atoms/RelativeTime/RelativeTime'
import IsAuthorized from '../../molecules/IsAuthorized/IsAuthorized'

import { resolvePath } from '../../../lib/object.helpers'
import { TextInputWithClipboard } from '../../atoms/ClipboardFields/ClipboardFields'
import TruncatedText from '../../atoms/TruncatedText/TruncatedText'
import { GetPvrURL, GetWwwURL } from '../../../lib/const.helpers'
import DynamicProgressMessages from '../../molecules/DynamicProgressMessages/DynamicProgressMessages'

function UserDeviceInfo (props) {
  const { deviceId, device, username, token, disabled } = props
  const { history = {}, summary = {} } = device

  const currentStep = history.currentStep
  const status = currentStep
    ? resolvePath(currentStep, 'progress.status', '')
    : resolvePath(summary, 'status', '')
  const statusMsg = currentStep
    ? resolvePath(currentStep, 'progress.status-msg', '')
    : resolvePath(summary, 'status-msg', '')
  const sha = currentStep
    ? resolvePath(currentStep, 'state-sha', '')
    : resolvePath(summary, 'state-sha', '')
  const revision = currentStep ? currentStep.rev : null
  const lastSeen = summary.timestamp
  const lastModified = summary['step-time']
  const cloneURL = history.lastRevision === revision
    ? `${GetPvrURL()}/${device['owner-nick']}/${device.nick}`
    : `${GetPvrURL()}/${device['owner-nick']}/${device.nick}/${revision}`

  const shareURL = history.lastRevision === revision
    ? `${GetWwwURL()}/u/${device['owner-nick']}/devices/${deviceId}`
    : `${GetWwwURL()}/u/${device['owner-nick']}/devices/${deviceId}/step/${revision}`

  return (
    <div className="device-detail-main">
      <div className="jumbotron">
        <div className="row">
          <div className="col-md-8">
            <div className="table-responsive">
              <table className="table table-borderless table-sm borderless table-light-header">
                <tbody>
                  <tr>
                    <th>Device ID</th>
                    <td className="text-monospace">{deviceId}</td>
                  </tr>
                  {revision !== null && (
                    <React.Fragment>
                      <tr key="revision">
                        <th>Commit ID (Rev)</th>
                        <td className="text-monospace">
                          {sha
                            ? (
                              [
                                <span className="progress-revision-commit" key="progress-revision-commit">
                                  <TruncatedText text={sha} size={8} appendText=" "/>
                                </span>,
                                <span className="progress-revision-rev text-muted" key="progress-revision-rev">
                                ({revision})
                                </span>
                              ]
                            )
                            : (
                              <span className="progress-revision-rev">
                                {revision}
                              </span>
                            )}
                        </td>
                      </tr>
                      <tr key="cloneURL">
                        <th>Clone URL</th>
                        <td>
                          <TextInputWithClipboard
                            value={cloneURL}
                            label="Clone URL"
                            disabled={disabled}
                          />
                        </td>
                      </tr>
                      <tr key="shareURL">
                        <th>Share URL</th>
                        <td>
                          <TextInputWithClipboard
                            value={shareURL}
                            label="Share URL"
                            disabled={disabled}
                          />
                        </td>
                      </tr>
                    </React.Fragment>
                  )}
                </tbody>
              </table>
            </div>
          </div>
          <div className="col-md-4">
            <div className="table-responsive">
              <table className="table table-borderless table-sm table-light-header">
                <tbody>
                  <IsAuthorized owner={device.owner}>
                    <tr>
                      <th>Actions</th>
                      <td>
                        <div className="btn-group">
                          <DevicePublishButton
                            username={username}
                            token={token}
                            deviceId={deviceId}
                            isPublic={device.public || false}
                            disabled={disabled}
                          />
                          <DeviceDeleteButton
                            username={username}
                            token={token}
                            deviceId={deviceId}
                            label={'Delete'}
                            disabled={disabled}
                          />
                        </div>
                      </td>
                    </tr>
                  </IsAuthorized>
                  {(status !== null || statusMsg !== null) && (
                    <tr>
                      <th>Status</th>
                      <td>
                        {currentStep !== null && (
                          <DynamicProgressMessages
                            status={status}
                            statusMsg={statusMsg}
                            currentStep={currentStep}
                          />
                        )}
                      </td>
                    </tr>
                  )}
                  <tr>
                    <th>Last Modified</th>
                    <td>
                      <strong>
                        <RelativeTime when={lastModified} />
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <th>Last Seen</th>
                    <td>
                      <strong>
                        <RelativeTime when={lastSeen} />
                      </strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserDeviceInfo
