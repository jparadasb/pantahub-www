/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { Link, withRouter, NavLink } from 'react-router-dom'

import { devicePatch } from '../../../store/devices/actions'
import { userDashboardPath } from '../../../router/routes'
import { classForDevStatus } from '../../../lib/utils'
import EditableElement from '../../atoms/Editable/Editable'
import { tabs } from '../../organisms/Navigator'
import { resolvePath } from '../../../lib/object.helpers'
import { SignedIcon } from '../../atoms/SignedIcon/SignedIcon'

class UserDeviceHeader extends PureComponent {
  handleStepNavigation = (event) => {
    const { username, deviceId, history, location, match } = this.props
    const restOfRoute = location.pathname.replace(match.url, '')
    history.push(`${userDashboardPath}/${username}/devices/${deviceId}/step/${event.target.value}${restOfRoute}`)
  }

  renderStepSelector = () => {
    const { device } = this.props
    const { history = {} } = device
    const { steps = [], revision } = history

    if (steps.length > 1) {
      return (
        <li className="page-item active">
          <select className="form-control" value={revision} onChange={this.handleStepNavigation}>
            {steps.map((_, index) => (<option key={index} value={index}>{index}</option>))}
          </select>
        </li>
      )
    }

    return (
      <li className="page-item active">
        <span className="page-link">
          {revision}
          <span className="sr-only">(current)</span>
        </span>
      </li>
    )
  }

  onEditSave = (nick) => {
    const {
      token,
      deviceId
    } = this.props
    this.props.devicePatch(token, deviceId, { nick })
  }

  render () {
    const {
      username,
      device = {},
      deviceId,
      disabled,
      editigHandler,
      saving,
      location,
      match
    } = this.props
    const { nick, history = {} } = device
    const { currentStep, steps = [], revision, revIndex, lastRevision } = history
    const prevIndex = revIndex > 0 ? revIndex - 1 : revIndex
    const nextIndex = revIndex < steps.length ? revIndex + 1 : revIndex
    const prevStep = steps[prevIndex] || {}
    const nextStep = steps[nextIndex] || {}

    const status = currentStep
      ? currentStep.progress
        ? currentStep.progress.status
        : null
      : null
    const isLast = revision === lastRevision
    const isFirst = revision === 0
    const restOfRoute = location.pathname.replace(match.url, '')
    return (
      <div className="header-sector">
        <div className="row">
          <div className="col-md-8">
            <div className="device-nick">
              <h1 className='d-flex align-items-center'>
                {resolvePath(device, 'history.currentStep.signatures.include', []).length > 0 && (
                  <SignedIcon
                    style={{ fontSize: '2.2rem', position: 'relative', top: '0.2rem' }}
                  />
                )}
                <EditableElement
                  el="span"
                  name="device-nick"
                  value={nick || deviceId}
                  editigHandler={editigHandler}
                  saving={saving}
                  slugify={true}
                  truncate={true}
                  saveHandler={this.onEditSave}
                />
              </h1>
              {device.public && (
                <i className="mdi mdi-earth" aria-hidden="true" />
              )}
              {status && (
                <span
                  className={`status-badge status-${status.toLowerCase()} badge badge-${classForDevStatus(
                    status
                  )}`}
                  style={{ marginTop: '-0.6em' }}
                >
                  {status}
                </span>
              )}
            </div>
          </div>
          <div className="col-md-4 text-right">
            <div className="device-revision-pager d-flex justify-content-end align-items-center">
              <strong>Revision:</strong>
              <nav
                className="d-inline-block"
                aria-label="Revisions navigator"
              >
                <ul className="pagination">
                  <li className="page-item">
                    <Link
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/0${restOfRoute}`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-double-left"></i>
                      </span>
                      <span className="sr-only"> First</span>
                    </Link>
                  </li>
                  <li className="page-item">
                    <Link
                      disabled={isFirst || disabled}
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/${
                        prevStep.rev || 0
                      }${restOfRoute}`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-left"></i>
                      </span>
                      <span className="sr-only"> Previous</span>
                    </Link>
                  </li>
                  {this.renderStepSelector()}
                  <li className="page-item">
                    <Link
                      disabled={isLast || disabled}
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/${
                        nextStep.rev || lastRevision
                      }${restOfRoute}`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-right"></i>
                      </span>
                      <span className="sr-only">Next </span>
                    </Link>
                  </li>
                  <li className="page-item">
                    <Link
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}${restOfRoute}`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-double-right"></i>
                      </span>
                      <span className="sr-only">Last</span>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 pt-4 pb-4">
            {(
              this.props.match.path === '/u/:username/devices/:deviceId' ||
              this.props.match.path === '/u/:username/devices/:deviceId/step/:revision'
            ) && (
              <ul className="nav nav-pills">
                {tabs.map(tab => (
                  <li className="nav-item" key={tab.id}>
                    <NavLink
                      exact={tab.exact}
                      to={`${this.props.match.url}${tab.id}`}
                      className="nav-link"
                      activeClassName="active"
                      id={`tab-${tab.id}`}
                      event="pageview"
                      payload={{ 'Page Path': 'user/device/tab-navigation', value: `${tab.id}` }}
                      aria-controls={`tab-${tab.id}`}
                    >
                      {tab.label}
                    </NavLink>
                  </li>
                ))}
              </ul>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(null, { devicePatch })(withRouter(UserDeviceHeader))
