/*
 * Copyright (c) 2017-2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { EditableMeta } from '../../molecules/EditableMeta/EditableMeta'
import { getProfileMetaData, putProfileMetaData } from '../../../services/profiles.service'
import SideMenuLayout from '../../layouts/SideMenuLayout'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../../router/routes'
import Loading from '../../atoms/Loading/Loading'

class ProfileGlobalConfig extends Component {
  state = {
    editedMeta: null,
    editing: false,
    loading: false,
    newKey: '',
    newValue: '',
    meta: {}
  }

  async componentDidMount (props) {
    this.setState({ loading: true })
    try {
      const response = await getProfileMetaData(this.props.token)
      if (!response.ok) {
        this.setState({ error: response.json, loading: false })
      }
      this.setState({ meta: response.json, loading: false })
    } catch (e) {
      this.setState({ error: e, loading: false })
    }
  }

  saveHandler = async (oldKey, newKey, oldValue, newValue) => {
    const fullMeta = {
      ...this.state.meta,
      ...(this.editedMeta || {})
    }
    delete fullMeta[oldKey]
    this.setState({
      editedMeta: { ...fullMeta }
    })
    try {
      fullMeta[newKey] = newValue
      const response = await putProfileMetaData(fullMeta, this.props.token)
      if (!response.ok) {
        return this.setState({ error: response.json, loading: false })
      }

      this.setState({
        editedMeta: response.json,
        meta: response.json,
        editing: false,
        loading: false
      })
    } catch (e) {
      return this.setState({ error: e, loading: false })
    }
  }

  editHandler = edit => {
    this.setState({
      editing: edit
    })
  }

  deleteHandler = async key => {
    const fullMeta = {
      ...this.state.meta,
      ...(this.editedMeta || {})
    }
    delete fullMeta[key]
    this.setState({
      editedMeta: fullMeta
    })
    try {
      const response = await putProfileMetaData(fullMeta, this.props.token)
      if (!response.ok) {
        return this.setState({ error: response.json, loading: false })
      }

      this.setState({
        editedMeta: response.json,
        meta: response.json,
        editing: false,
        loading: false
      })
    } catch (e) {
      return this.setState({ error: e, loading: false })
    }
  }

  addHandler = async () => {
    const fullMeta = {
      ...this.state.meta,
      ...(this.editedMeta || {})
    }
    const editedMeta = {
      ...fullMeta,
      [this.state.newKey]: this.state.newValue
    }
    this.setState({
      editedMeta: editedMeta
    })
    if (this.state.newKey && this.state.newValue) {
      try {
        const response = await putProfileMetaData(editedMeta, this.props.token)
        if (!response.ok) {
          return this.setState({ error: response.json, loading: false })
        }

        this.setState({
          newKey: '',
          newValue: '',
          editedMeta: response.json,
          meta: response.json,
          editing: false,
          loading: false
        })
      } catch (e) {
        return this.setState({ error: e, loading: false })
      }
    }
  }

  newValueHandler = evt => this.setState({ newValue: evt.target.value })
  newKeyHandler = evt => this.setState({ newKey: evt.target.value })

  render () {
    return (
      <SideMenuLayout
        sidecontent={null}
        header={
          <div
            key="breadcrumbs"
            className="breadcrumb-sector row align-items-center"
          >
            <div className="col-12">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to={`${userDashboardPath}/user`}>Dashboard</Link>
                </li>
                <li className="breadcrumb-item active">Settings</li>
              </ol>
            </div>
          </div>
        }
        content={
          <section>
            <header className="pb-4">
              <h1 className="font-weight-normal">
                Global Configuration
              </h1>
            </header>
            {this.state.loading && (
              <Loading />
            )}
            {!this.state.loading && this.state.error && (
              <h5 className="text-center">Error getting global configuration</h5>
            )}
            {!this.state.loading && !this.state.error && (
              <section>
                <EditableMeta
                  title=''
                  meta={this.state.meta}
                  editedMeta={this.state.editedMeta}
                  loading={this.state.loading}
                  editing={this.state.editing}
                  newKey={this.state.newKey}
                  newValue={this.state.newValue}
                  saveHandler={this.saveHandler}
                  editHandler={this.editHandler}
                  deleteHandler={this.deleteHandler}
                  addHandler={this.addHandler}
                  newKeyHandler={this.newKeyHandler}
                  newValueHandler={this.newValueHandler}
                />
              </section>
            )}
          </section>
        }
      />
    )
  }
}

export default connect(
  (state) => ({
    token: state.auth.token
  }),
  {}
)(ProfileGlobalConfig)
