/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import { Doughnut } from 'react-chartjs-2'

export default class UserDashboardQuotaDoughnut extends Component {
  render () {
    const { actual, max } = this.props
    const pct = actual / max
    const available = max - actual
    const data = {
      labels: ['Actual', 'Available'],
      datasets: [
        {
          data: [actual, available < 0 ? 0 : available],
          backgroundColor: [pct >= 0.75 ? '#ff5d5d' : '#8BD2E6', '#f2f2f2']
        }
      ]
    }

    return (
      <Doughnut
        data={data}
        legend={{ display: false }}
        options={{
          cutoutPercentage: 80,
          layout: {
            padding: 10
          }
        }}
        width={171}
        height={171}
      />
    )
  }
}
