/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { userDashboardPath } from '../../../router/routes'
import ProfileForm from '../../organisms/ProfileForm/ProfileForm'
import { FullHeightLoading } from '../../../router'
import { getProfile, updateApp } from '../../../store/profile/actions'
import { STATES, DefaultProfile } from '../../../store/profile/reducer'
import SideMenuLayout from '../../layouts/SideMenuLayout'
// import SettingsMenu from '../../molecules/SettingsMenu/SettingsMenu'

class EditProfile extends React.Component {
  componentDidMount () {
    if (
      this.props.profile.current.nick === '' &&
      this.props.profile.status &&
      this.props.profile.status !== STATES.GET_PROFILE.IN_PROGRESS
    ) {
      this.props.getProfile(this.props.nick)
    }
  }

  onUpdate = (profile) => {
    this.props.updateApp(profile)
  }

  render () {
    return (
      <SideMenuLayout
        sidecontent={null}
        header={
          <div
            key="breadcrumbs"
            className="breadcrumb-sector row align-items-center"
          >
            <div className="col-12">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to={`${userDashboardPath}/user`}>Dashboard</Link>
                </li>
                <li className="breadcrumb-item">Settings</li>
                <li className="breadcrumb-item active">Profile</li>
              </ol>
            </div>
          </div>
        }
        content={
          <div className="edit-profile">
            <section>
              <header>
                <h1>Edit your profile </h1>
                <small>{this.props.auth.prn}</small>
              </header>
              {this.props.profile.status === STATES.GET_PROFILE.IN_PROGRESS
                ? (
                  <FullHeightLoading />
                )
                : (
                  <>
                    <ProfileForm
                      profile={{ ...DefaultProfile, ...this.props.profile.current }}
                      loading={this.props.profile.status === STATES.PUT_PROFILE.IN_PROGRESS}
                      onUpdate={this.onUpdate}
                    />
                  </>
                )}
            </section>
          </div>
        }
      />
    )
  }
}

export default connect(
  (state) => ({
    nick: state.auth.nick,
    profile: state.profile
  }),
  {
    getProfile,
    updateApp
  }
)(EditProfile)
