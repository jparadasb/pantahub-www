/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { userDashboardPath } from '../../../router/routes'
import ClaimDeviceForm from '../../organisms/ClaimDeviceForm/ClaimDeviceForm'
import ClaimDeviceCordova from '../../organisms/ClaimDeviceForm/ClaimDeviceCordova'
import { isMobile } from '../../../lib/native'

function ClaimDevice ({ auth: { username }, claims: { manual = false } }) {
  return (
    <React.Fragment>
      <div
        key="breadcrumbs"
        className="breadcrumb-sector row align-items-center"
      >
        <div className="col-6">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to={`${userDashboardPath}/${username}`}>Dashboard</Link>
            </li>
            <li className="breadcrumb-item active">
              <Link to={`${userDashboardPath}/${username}/devices`}>
                Devices
              </Link>
            </li>
            <li className="breadcrumb-item active">Claim Device</li>
          </ol>
        </div>
      </div>
      <br />
      <div key="form" className="row justify-content-center">
        <div className="col-md-6">
          <div className="form-registration-box">
            <div className="card">
              <div className="card-body">
                <h1>Claim Devices</h1>
                {isMobile() && (<ClaimDeviceCordova />)}
                {(manual || !isMobile()) && (<ClaimDeviceForm />)}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default connect(
  state => state,
  null
)(ClaimDevice)
