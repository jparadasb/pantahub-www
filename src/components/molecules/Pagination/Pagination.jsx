import React from 'react'
import PropTypes from 'prop-types'

class Pagination extends React.Component {
  pagesSizes = [
    10,
    20,
    30,
    50,
    100
  ]

  maxPage = () => {
    return Math.ceil(this.props.pagination.total / this.props.pagination.page_size)
  }

  lastOffset = () => {
    return (this.maxPage() * this.props.pagination.page_size) - this.props.pagination.page_size
  }

  pagesizeFounded = () => {
    return this.pagesSizes.indexOf(this.props.pagination.page_size) >= 0
  }

  changePageSize = (event) => {
    this.props.onPaginationChange({
      ...this.props.pagination,
      resource: undefined,
      next: undefined,
      prev: undefined,
      current_page: undefined,
      total: undefined,
      page_size: event.target.value
    })
  }

  getSorting = () => {
    const urlParms = new URLSearchParams(window.location.search)
    return urlParms.get('sort_by')
  }

  render () {
    const sorting = this.getSorting()
    const firstUrl = `${this.props.pagination.resource.replace('/api/v1', '')}?page[size]=${this.props.pagination.page_size}&page[offset]=0${sorting ? '&sort_by=' + sorting : ''}`
    const lastUrl = `${this.props.pagination.resource.replace('/api/v1', '')}?page[size]=${this.props.pagination.page_size}&page[offset]=${this.lastOffset()}${sorting ? '&sort_by=' + sorting : ''}`

    return (
      <div className="d-flex align-items-center justify-content-end">
        <div className="d-flex  align-items-center ">
          <div className="mr--1">
            Total: {this.props.pagination.total}
          </div>
          <div className="mr--1">
            <span className="mr--1">Page size:</span>
            <select
              name="page_size"
              id="page_size"
              defaultValue={this.props.pagination.page_size}
              onChange={this.changePageSize}
            >
              {this.pagesSizes.map(size => (
                <option
                  key={size}
                  value={size}
                >
                  {size}
                </option>
              ))}
              {!this.pagesizeFounded() && (
                <option
                  key={this.props.pagination.page_size}
                  value={this.props.pagination.page_size}
                >
                  {this.props.pagination.page_size}
                </option>
              )}
            </select>
          </div>
        </div>
        <nav className="">
          <ul className="pagination" style={{ padding: 0, margin: 0 }}>
            <li className="page-item">
              <a
                className="page-link"
                href={firstUrl}
                aria-label="Previous"
              >
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            {(this.props.pagination.current_page) > 2 && (
              <li className="page-item">
                <a
                  className="page-link"
                  href={this.props.pagination.prev.replace('/api/v1', '')}
                >
                  &lsaquo;
                </a>
              </li>
            )}
            {this.props.pagination.current_page > 1 && (
              <li className="page-item">
                <a
                  className="page-link"
                  href={this.props.pagination.prev.replace('/api/v1', '')}
                >
                  {this.props.pagination.current_page - 1}
                </a>
              </li>
            )}
            <li className="page-item active">
              <a
                className="page-link disabled"
                href="#"
              >
                {this.props.pagination.current_page}
              </a>
            </li>
            {this.props.pagination.current_page < this.maxPage() && (
              <li className="page-item">
                <a
                  className="page-link"
                  href={this.props.pagination.next.replace('/api/v1', '')}
                >
                  {this.props.pagination.current_page + 1}
                </a>
              </li>
            )}
            {this.props.pagination.current_page < this.maxPage() &&
              this.props.pagination.current_page + 1 < this.maxPage() && (
              <li className="page-item">
                <a
                  className="page-link"
                  href={this.props.pagination.next.replace('/api/v1', '')}
                >
                  &rsaquo;
                </a>
              </li>
            )}
            <li className="page-item">
              <a
                className="page-link"
                href={lastUrl}
                aria-label="Last">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

const IPaginationType = PropTypes.shape({
  resource: PropTypes.string,
  page_size: PropTypes.number,
  page_offset: PropTypes.number,
  current_page: PropTypes.number,
  total: PropTypes.number,
  next: PropTypes.string,
  prev: PropTypes.string,
  sorting: PropTypes.object
})

Pagination.propTypes = {
  pagination: IPaginationType,
  onPaginationChange: PropTypes.func
}

export default Pagination
