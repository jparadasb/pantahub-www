import React from 'react'

export function SignedIcon (props) {
  return (
    <i
      className={`mdi mdi-seal ${props.className || 'text-purple'}`}
      title="Platform signed"
      {...props}
    />
  )
}
