import React from 'react'
import Dinero from 'dinero.js'

export function Money ({ amount = 0, currency = 'USD' }) {
  const price = Dinero({
    amount: amount,
    currency: currency === '' ? 'USD' : currency
  })

  return (
    <>
      {price.toFormat()}
    </>
  )
}
