import React from 'react'
import uuid from 'uuid'

export default function CheckInput (props) {
  const {
    id = uuid.v4(),
    controlled = true,
    onChange = () => {},
    value,
    disabled
  } = props

  const name = props.name || id
  const onChangeHandler = (evnt) => {
    onChange(evnt.target.checked)
  }

  return (
    <div className="form-check">
      <input
        className="form-check-input"
        name={name}
        type="checkbox"
        checked={value}
        onChange={controlled ? onChangeHandler : onChange }
        id={id}
        disabled={disabled}
      />
      <label
        className="form-check-label"
        htmlFor={id}>
        {props.children}
      </label>
    </div>
  )
}
