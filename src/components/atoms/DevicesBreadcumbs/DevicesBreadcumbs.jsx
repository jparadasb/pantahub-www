/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { userDashboardPath } from '../../../router/routes'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

function Breadcumbs ({ auth = {}, username, deviceNick, steps }) {
  return (
    <div
      key="breadcrumbs"
      className="breadcrumb-sector row align-items-center"
    >
      <div className="col-12">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            {(auth.nick && auth.nick === username)
              ? <Link to={`${userDashboardPath}/${username}`}>Dashboard</Link>
              : username
            }
          </li>
          {username && (
            <li className="breadcrumb-item">
              <Link to={`${userDashboardPath}/${username}/devices`}>Devices</Link>
            </li>
          )}
          {deviceNick && (
            <li className="breadcrumb-item">
              {deviceNick}
            </li>
          )}
          {steps && steps.length > 0 && (
            steps.map((step, id) => (
              <li key={id} className="breadcrumb-item">
                {step.to
                  ? (
                    <Link to={step.to}>
                      {step.title}
                    </Link>
                  )
                  : (
                    step.title
                  )}
              </li>
            ))
          )}
        </ol>
      </div>
    </div>
  )
}

export default connect(
  state => ({
    auth: state.auth
  })
)(Breadcumbs)
