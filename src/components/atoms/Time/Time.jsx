import React from 'react'
import dayjs from 'dayjs'

export default function Time ({ when, format = 'DD MMM YYYY' }) {
  const date = dayjs(when)
  return (
    <time dateTime={date.format()}>
      <span title={date.format()}>
        {date.format(format)}
      </span>
    </time>
  )
}
