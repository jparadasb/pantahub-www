/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import ValidaJs from 'valida-js'
import { FileSize, ValidateIn } from '../../../lib/validators.helpers'

const APP_LOGO_MAX_SIZE_KB = Number(
  window.env.REACT_APP_3RD_PARTY_APP_LOGO_MAX_SIZE_KB || '15'
)

ValidaJs.validators.in = ValidateIn
ValidaJs.validators.size = FileSize

const rules = ValidaJs.rulesCreator(ValidaJs.validators, [
  {
    name: 'type',
    type: 'required',
    stateMap: 'type'
  },
  {
    name: 'type',
    type: 'in',
    stateMap: 'type',
    compareWith: ['public', 'confidential']
  },
  {
    name: 'nick',
    type: 'required',
    stateMap: 'nick'
  },
  {
    name: 'scopes',
    type: 'minLength',
    stateMap: 'scopes',
    compareWith: 1
  },
  {
    name: 'redirect_uris',
    type: 'minLength',
    stateMap: 'redirect_uris',
    compareWith: 1
  },
  {
    name: 'logo',
    type: 'size',
    stateMap: 'logo',
    compareWith: APP_LOGO_MAX_SIZE_KB
  }
])

export const validate = (state) => ValidaJs.validate(rules, state)
