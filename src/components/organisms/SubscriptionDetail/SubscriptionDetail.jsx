import React from 'react'
import { useSelector } from 'react-redux'
import { Money } from '../../atoms/Money/Money'
import { DateFromStamp } from '../../atoms/DateFromStamp/DateFromStamp'
import UserDashboardSubscriptionCharts from '../../pages/UserDashboard/UserDashboardSubscriptionCharts'
import { resolvePath } from '../../../lib/object.helpers'
import { SubscriptionDayleft } from '../../pages/UserDashboard/SubscriptionDayleft'

export function SubscriptionDetail () {
  const data = useSelector((state) => ({
    subscription: state.billing.subscriptions.current,
    sQuota: state.dash.data.subscription
  }))

  if (!data.subscription) {
    return null
  }

  const nextInvoice = resolvePath(data, 'subscription.next_invoice', '')
  const lastestInvoice = resolvePath(data, 'subscription.lastest_invoice', { lines: { data: [] } })

  return (
    <section className='row pt-40 pb-40'>
      {lastestInvoice && nextInvoice && (
        <div className="col-md-7 pr-4">
          {lastestInvoice && lastestInvoice.lines.data.length > 0 && (
            <div className=''>
              <h5 className='fw-normal'>
                Last invoice
              </h5>
              <div className='mb-0'>
                <table className='table'>
                  <thead>
                    <tr>
                      <th></th>
                      <th>Description</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    {lastestInvoice.lines.data.map((l) => (
                      <tr key={l.id}>
                        <td className='text-muted'>
                          <DateFromStamp when={l.period.start} format="DD MMM" />
                          &nbsp;-&nbsp;
                          <DateFromStamp when={l.period.end} format="DD MMM" />
                        </td>
                        <td>{l.description}</td>
                        <td>
                          <Money amount={l.amount} currency={l.currency} />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          )}
          {nextInvoice && nextInvoice.length > 0 && (
            <div className='mt-4'>
              <h5 className='fw-normal'>
                Upcoming invoice
              </h5>
              <table className='table'>
                <thead>
                  <tr>
                    <th></th>
                    <th>Description</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  {nextInvoice.map((l) => (
                    <tr key={l.id}>
                      <td className='text-muted'>
                        <DateFromStamp when={l.period.start} format="DD MMM" />
                        &nbsp;-&nbsp;
                        <DateFromStamp when={l.period.end} format="DD MMM" />
                      </td>
                      <td>
                        {l.description}
                      </td>
                      <td>
                        <Money
                          amount={l.amount}
                          currency={l.currency}
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      )}
      <div className="col-md-5">
        <div className="card card-with-bg">
          <div className="card-body quota-box">
            <h4 className='card-title'>Your quota</h4>
            <UserDashboardSubscriptionCharts
              subscription={data.sQuota}
              loading={false}
            />
            <div className="row">
              <div className="col-12 text-center mb-4 mt-2">
                <SubscriptionDayleft />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
