/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component, Fragment, useEffect, useState } from 'react'

import { resolvePath } from '../../../lib/object.helpers'
import { Link, withRouter, Redirect } from 'react-router-dom'
import { ReadStateFolder } from './functions'
import { JSONDisplayWithClipboard, TextDisplayWithClipboard } from '../../atoms/ClipboardFields/ClipboardFields'

import './files.scss'
import Loading from '../../atoms/Loading/Loading'
import fetch from 'isomorphic-fetch'
import { SignedIcon } from '../../atoms/SignedIcon/SignedIcon'

const defaultDescription = {
  extension: 'folder',
  size: null
}

const defaultFile = {
  url: '',
  size: 0,
  extension: '',
  isExpandable: false,
  hidden: false,
  value: ''
}

const extensionToIcon = {
  json: 'mdi-code-json',
  conf: 'mdi-cog',
  text: 'mdi-code-string',
  config: 'mdi-code-string',
  txt: 'mdi-code-string',
  c: 'mdi-language-c',
  cpp: 'mdi-language-cpp',
  csharp: 'mdi-language-csharp',
  css: 'mdi-language-css3',
  fortran: 'mdi-language-fortran',
  go: 'mdi-language-go',
  hs: 'mdi-language-haskell',
  html: 'mdi-language-html5',
  java: 'mdi-language-java',
  class: 'mdi-language-java',
  javascript: 'mdi-language-javascript',
  js: 'mdi-language-javascript',
  kotlin: 'mdi-language-kotlin',
  lua: 'mdi-language-lua',
  markdown: 'mdi-language-markdown',
  md: 'mdi-language-markdown',
  php: 'mdi-language-php',
  python: 'mdi-language-python',
  r: 'mdi-language-r',
  ruby: 'mdi-language-ruby',
  rb: 'mdi-language-ruby',
  rust: 'mdi-language-rust',
  swift: 'mdi-language-swift',
  typescript: 'mdi-language-typescript',
  ts: 'mdi-language-typescript',
  xml: 'mdi-language-xaml',
  xaml: 'mdi-language-xaml',
  folder: 'mdi-folder',
  default: 'mdi-file'
}

const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
const k = 1024
const maxViewableFileSize = k * 100

class FilesFromState extends Component {
  state = {
    ls: {}
  }

  componentDidMount () {}

  render () {
    const { device, disabled, match, location } = this.props
    const { state, rawState } = resolvePath(device, 'history.currentStep', {})

    const route = location.pathname.split(match.url)[1].split(/\/-\/?/)[1]
    if (!rawState || !state) {
      return (
        <Loading />
      )
    }

    if (!state[route]) {
      const folder = ReadStateFolder(rawState, route)
      if (folder.length === 0) {
        return <Redirect to={match.url} />
      }

      return (
        <div className="files-navigator table-responsive">
          <FolderBreadCrumbs url={match.url} route={route} />
          <table className="table table-hover table-responsive table-no-first-border">
            <thead>
              <tr>
                <th scope="col" style={{ width: '30px' }}>{' '}</th>
                <th scope="col">Name</th>
                <th scope="col" style={{ width: '140px' }}>Size</th>
              </tr>
            </thead>
            <tbody>
              {folder.map((desc) => (
                <FileRow
                  k={desc.key}
                  key={desc.key}
                  {...desc}
                  description={state[RouteFromKey(route, desc.key)]}
                  url={location.pathname}
                  rawState={rawState}
                  disabled={disabled}
                />
              ))}
            </tbody>
          </table>
        </div>
      )
    }

    return (
      <div className="files-navigator table-responsive">
        <FolderBreadCrumbs url={match.url} route={route} />
        <FileContent file={state[route]} route={route} />
      </div>
    )
  }
}

function RouteFromKey (baseRoute, key) {
  return baseRoute ? `${baseRoute}/${key}` : key
}

function FolderBreadCrumbs ({ url, route = '' }) {
  let prev = `${url}/-/`
  const routes = route.split('/').reduce((acc, r) => {
    if (r === '') {
      return acc
    }

    const newR = `${prev}/${r}`.replace('//', '/')
    prev = newR
    acc.push({ to: newR, value: r })

    return acc
  }, [{ to: prev, value: 'Files' }])
  return (
    <div style={{ paddingTop: '1em', paddingBottom: '1em' }}>
      {routes.map((route, i) => (
        <React.Fragment key={`${route.value}--${i}`} >
          <Link to={route.to}>
            {route.value}
          </Link>
          {i < routes.length - 1 && (' / ')}
        </React.Fragment>
      ))}
    </div>
  )
}

function FileContent ({ file = defaultFile, route }) {
  if (file.name === '') {
    return null
  }

  const routes = route.split('/')
  const name = routes.length > 0 ? routes[routes.length - 1] : routes[0]

  if (file.extension === 'json') {
    return (
      <JSONDisplayWithClipboard value={file.value} disabled={false} />
    )
  }

  if (extensionToIcon[file.extension]) {
    return <DownloadContent file={file} route={route} />
  }

  if ((file.extension === name || file.extension === route) && file.size < maxViewableFileSize) {
    return <DownloadContent file={file} route={route} />
  }

  return (<DownloadLink url={file.url} name={name} />)
}

function DownloadLink ({ url, name }) {
  return (
    <div className="d-flex justify-content-center pt-6 pb-6 download-file">
      <a
        className="font-weight-normal d-flex flex-column align-items-center justify-content-center"
        href={url}
      >
        <i className="mdi mdi-cloud-download text-muted" aria-hidden="true" />
        <br />
        <span className="font-weight-normal">Download {name} content</span>
      </a>
    </div>
  )
}
function DownloadContent ({ file, route }) {
  const [content, setContent] = useState(null)
  const [download, setDownload] = useState({ url: null, name: null })

  useEffect(() => {
    fetch(file.url)
      .then((response) => {
        return response.ok
          ? response.blob()
          : Promise.reject(new Error('Can load file'))
      })
      .then((blob) => {
        // eslint-disable-next-line no-console
        console.log(blob)
        if (blob.type.indexOf('text') >= 0) {
          return blob
        } else {
          const routes = route.split('/')
          const name = routes.length > 0 ? routes[routes.length - 1] : routes[0]
          setDownload({ url: file.url, name: name })
          return Promise.reject(new Error('File not suported'))
        }
      })
      .then((blob) => blob.text())
      .then((text) => setContent(text))
      .catch(console.info)
  }, [])

  if (!content && !download.url && !download.name) {
    return (
      <Loading />
    )
  }

  if (content) {
    return (
      <TextDisplayWithClipboard value={content} disabled={false} />
    )
  }

  return (<DownloadLink url={download.url} name={download.name} />)
}

function FileRow ({ sigs, k, url, description = defaultDescription, disabled }) {
  const baseUrl = url
  if (url.indexOf('/-') < 0) {
    url = url + '/-'
  }
  url = `${url}/${k}`.replace('//', '/')
  return (
    <Fragment>
      <tr key={k} style={{ verticalAlign: 'middle' }}>
        <td>
          <IconFileType
            style={{ position: 'relative', top: '2px' }}
            extension={description.extension}
          />
        </td>
        <td className='d-flex align-items-center'>
          <Link
            style={{ display: 'block' }}
            className="pt-2 pb-2"
            to={url}
          >
            {k}
          </Link>
          <div style={{ padding: '0 0.5em' }}>
            <SignaturesLink
              url={baseUrl}
              signatures={sigs}
            />
          </div>
        </td>
        <td>
          <FileSize bytes={description.size} />
        </td>
      </tr>
    </Fragment>
  )
}

function SignaturesLink ({ url, signatures = [] }) {
  url = url.split('/-')[0]
  if (url.indexOf('/-') < 0) {
    url = url + '/-'
  }

  return (
    <>
      {signatures.map((s) => (
        <Link
          key={s}
          to={`${url}/${s}`.replace('//', '/')}
          style={{ position: 'relative', top: '2px' }}
        >
          <SignedIcon title={`Platform signed with ${s}`} />
        </Link>
      ))}
    </>
  )
}

export function IconFileType ({ extension = '', ...rest }) {
  const iconClass = !extensionToIcon[extension] ? extensionToIcon.default : extensionToIcon[extension]
  return (
    <i className={`mdi ${iconClass}`} aria-hidden="true" {...rest} ></i>
  )
}

export function FileSize ({ bytes = 0, decimals = 2 }) {
  if (bytes === 0 || !bytes) return null

  const size = GetFileSize(bytes, decimals)
  return (
    <span className="file-size">
      {size.toString()}
    </span>
  )
}

export function GetFileSize (bytes = 0, decimals = 2) {
  if (bytes === 0 || !bytes) {
    return new FSize()
  }

  return new FSize(bytes, decimals)
}

class FSize {
  constructor (bytes = 0, decimals = 2) {
    const dm = Math.max(decimals, 0)
    const i = Math.floor(Math.log(bytes) / Math.log(k))

    this.amount = parseFloat((bytes / Math.pow(k, i)).toFixed(dm))
    this.unit = sizes[i]
  }

  toString () {
    return `${this.amount} ${this.unit}`
  }
}

export default withRouter(FilesFromState)
