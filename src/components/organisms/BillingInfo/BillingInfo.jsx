import React from 'react'
import { useSelector } from 'react-redux'

export default function BillingInfo () {
  const profile = useSelector((state) => state.profile.current)
  return (
    <div className='pt-5 pb-5'>
      <h1>Billing Info</h1>
      <p>
        {profile.email}
      </p>
    </div>
  )
}
