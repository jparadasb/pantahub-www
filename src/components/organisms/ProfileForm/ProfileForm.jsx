/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useEffect, useCallback } from 'react'
import { connect } from 'react-redux'
import { validate } from './validation'
import TextInput from '../../atoms/TextInput/TextInput'
import FileUploadAndCrop from '../../molecules/FileUploadAndCrop/FileUploadAndCrop'
import TextArea from '../../atoms/TextArea/TextArea'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

const REACT_APP_PROFILE_PICTURE_MAX_SIZE_KB = Number(
  window.env.REACT_APP_3RD_PARTY_REACT_APP_PROFILE_PICTURE_MAX_SIZE_KB || '1000'
)

function ProfileForm ({ profile, loading, onUpdate }) {
  const [state, setState] = useState({ wasValidated: false, errors: [] })
  const [profileState, setProfile] = useState({ ...profile })

  useEffect(() => {
    setProfile(s => ({
      ...s,
      ...profile
    }))
    return () => {}
  }, [profile])

  const onChange = (input) => (event) => {
    setProfile(s => ({
      ...s,
      [input]: event.target.value
    }))
  }

  const submit = useCallback((event) => {
    event.preventDefault()
    const form = validate(profileState)
    if (form.valid) {
      onUpdate(profileState)
      return
    }

    const errors = Object.values(form.errors).reduce((acc, v) => [...acc, ...v], [])
    setState({ ...state, errors: errors })
  }, [state, profileState, onUpdate])

  const wasValidatedClass = state.wasValidated
    ? 'row was-validated'
    : 'row'
  return (
    <div className="pt-4 pb-8">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
      >
        <div className="col-md-4">
          <FileUploadAndCrop
            name="picture"
            type="file"
            message="Drag or click to upload profile picture"
            value={profileState.picture}
            onChange={onChange('picture')}
            maximunSize={REACT_APP_PROFILE_PICTURE_MAX_SIZE_KB}
            maxHeight={200}
            pictureStyle={{
              display: 'flex',
              alignItems: 'center'
            }}
            style={{
              width: '200px',
              height: '200px',
              display: 'flex',
              alignItems: 'center',
              padding: '0',
              marginTop: '0',
              overflow: 'hidden'
            }}
            cropperStyle={{
              width: '200px',
              height: '200px',
              margin: '0 auto'
            }}
          />
        </div>
        <div className="col-md-8">
          <TextInput
            label="Email"
            name="email"
            type="text"
            maxLength="20"
            placeholder="Email"
            disabled={true}
            value={profileState.email}
          />
          <TextInput
            label="Full name"
            name="fullName"
            type="text"
            maxLength="20"
            placeholder="Jhon Smith"
            value={profileState.fullName}
            onChange={onChange('fullName')}
          />
          <TextArea
            label="Biography"
            name="bio"
            maxLength="100"
            value={profileState.bio}
            onChange={onChange('bio')}
          />
          <TextInput
            label="Location"
            name="location"
            type="text"
            maxLength="50"
            placeholder="NY, USA"
            value={profileState.location}
            onChange={onChange('location')}
          />
          <TextInput
            label="Company name"
            name="company"
            type="text"
            maxLength="50"
            placeholder="Your company name"
            value={profileState.company}
            onChange={onChange('company')}
          />
          <TextInput
            label="Website URL"
            name="website"
            type="text"
            maxLength="50"
            placeholder="Your website URL"
            value={profileState.website}
            onChange={onChange('website')}
          />
          <TextInput
            label="Github URL"
            name="github"
            type="text"
            maxLength="50"
            placeholder="Your github URL"
            value={profileState.github}
            onChange={onChange('github')}
          />
          <TextInput
            label="Gitlab URL"
            name="gitlab"
            type="text"
            maxLength="50"
            placeholder="Your gitlab URL"
            value={profileState.gitlab}
            onChange={onChange('gitlab')}
          />
          <TextInput
            label="Twitter URL"
            name="twitter"
            type="text"
            maxLength="50"
            placeholder="Your twitter URL"
            value={profileState.twitter}
            onChange={onChange('twitter')}
          />
          <React.Fragment>
            {state.errors.map((error, id) => (
              <p key={id} className="alert alert-danger">
                {JSON.stringify(error)}
              </p>
            ))}
          </React.Fragment>
          <TrackedButton
            type="submit"
            className="btn btn-primary  btn-block mt-4"
            disabled={loading}
          >
            {loading
              ? (
                <span className="mdi mdi-refresh pantahub-loading" />
              )
              : (
                'Update your profile'
              )}
          </TrackedButton>
        </div>
      </form>
    </div>
  )
}

export default connect((state) => ({
  token: state.auth.token,
  nick: state.auth.nick
}))(ProfileForm)
