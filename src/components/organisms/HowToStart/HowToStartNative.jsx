/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../../router/routes'
import UserDashboardData from '../../pages/UserDashboard/UserDashboardData'
import './how_to_start.scss'

export default function HowToStartNative (props) {
  const { username, className, loading, topDevices, subscription } = props

  return (
    <div key="dashboard" className="">
      <div className={`hts getting-started ${className || ''}`}>
        <div className="bg-color bg-color__small-left bg-brown-light bg-color--small">
          <div className="container pb-1 pt-0">
            <div className="row justify-content-md-center">
              <div className="col-md-12">
                <h1>
                  Welcome to Pantahub!<br />
                  <small>Let&apos;s start connecting devices</small>
                </h1>
                <p className="pt-2 pb-2">
                  {`Just came here for the first time and wonder how to get started?
                Head over to `}
                  <a href="https://docs.pantahub.com">
                    our docs</a>
                  {` and follow one of the "Getting Started" guides to get going quickly.
                Let us know about what you have done and how things could be better.`}
                </p>
              </div>
            </div>
            <div className="row hts__steps pb-4 justify-content-md-center">
              <div className="col-md-12 mt-4 mb-4">
                <div className="row">
                  <div className="col-md-12">
                    <p>
                      <Link
                        className="btn btn-primary  btn-block"
                        to={`${userDashboardPath}/${username}/claim`}
                      >
                        Claim a Device</Link>
                    </p>
                    <p>See our <a href="https://docs.pantahub.com/claim-device-auto/" rel="noopener noreferrer" target="_blank">documentation</a> for more info</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <UserDashboardData
              username={username}
              loading={loading}
              topDevices={topDevices}
              subscription={subscription}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
