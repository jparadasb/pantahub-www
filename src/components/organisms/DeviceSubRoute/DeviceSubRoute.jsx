/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'

import { useRouteMatch, Route, Switch } from 'react-router'
import { tabs } from '../Navigator/index'

function DeviceSubRoute (props) {
  const { path } = useRouteMatch()
  const {
    navigator,
    activeTab,
    device,
    username,
    token,
    dispatch
  } = props

  return (
    <Switch>
      {tabs.map(tab => (
        <Route
          exact={tab.exact}
          path={`${path}${tab.id}`}
          key={tab.id}
          id={`tab-content-${tab.id}`}
          className={`tab-pane fade show ${
            tab.id === activeTab ? 'active' : ''
          }`}
        >
          <section
            key={tab.id}
            className="device-sub-route-content"
          >
            {tab.title && (
              <header>
                <h5 className="mb-4 font-weight-normal">{tab.title}</h5>
              </header>
            )}
            {tab.components.map((TabComponent, key) => {
              return (
                <TabComponent
                  key={key}
                  device={device}
                  username={username}
                  token={token}
                  dispatch={dispatch}
                  navigator={navigator}
                  {...props}
                />
              )
            })}
          </section>
        </Route>
      ))}
    </Switch>
  )
}

export default React.memo(DeviceSubRoute)
