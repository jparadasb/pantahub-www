import React from 'react'
import { getPlatform } from '../../lib/native'
import { Helmet } from 'react-helmet'
import Footer from '../molecules/Footer/Footer'
import { GetEnv } from '../../lib/const.helpers'
import HeaderNav from '../molecules/Nav/HeaderNav'
import SidebarNav from '../molecules/Nav/SidebarNav'

import './SidebarLayout.scss'

export const SidebarLayout = ({ children }) => {
  const classes = `${getPlatform()} d-flex`
  return (
    <React.Fragment>
      <Helmet>
        <title>{GetEnv('REACT_APP_TITLE', 'PantaHub')}</title>
      </Helmet>
      <header>
        <HeaderNav />
      </header>
      <div className={classes}>
        <SidebarNav />
        <div className="minimal-height sidebar-layout-content">
          <div className="container">
            {children}
          </div>
        </div>
      </div>
      <div>
        <footer>
          <Footer />
        </footer>
      </div>
    </React.Fragment>
  )
}
