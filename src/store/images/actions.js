
import { buildBasicActions } from '../../lib/redux.helpers'
import * as Types from './types'
import { getImagesData } from '../../services/images.service'
import { catchError } from '../general-flash/actions'
import { processService } from '../../services/api.service'
import { resolvePath } from '../../lib/object.helpers'
import { getFactoryToken } from '../../services/devices.service'
import { BuildURLWithParams } from '../../lib/api.helpers'
import { GetEnv } from '../../lib/const.helpers'

const getActions = buildBasicActions(Types, 'IMAGES_RELEASES_GET')

export const HTTPD_USER_USER_META_KEY = 'pvr-sdk.httpd.user'
export const HTTPD_PASS_USER_META_KEY = 'pvr-sdk.httpd.secret'
export const HTTPD_USER = 'admin'
export const HTTPD_PASS = 'cloudflare'
const defaultPayload = { 'default-user-meta': {} }
const defaultOptions = {
  device: '',
  devicenick: '',
  channel: 'stable',
  bucket: '',
  project: '',
  releaseindex: 'stable',
  payload: defaultPayload,
  variant: '',
  wifiConfig: '',
  cmdline: '',
  autoClaim: true,
  source: ''
}

const IMAGER_URL = 'https://images.apps.pantahub.com/get'

const getConfig = (factoryToken, wifiConfig = {}, extraCmdline = '') => {
  const host = new URL(GetEnv('REACT_APP_API_URL', 'https://api.pantahub.com')).hostname
  let config = `configargs=ph_creds.host=${host}`

  if (factoryToken) {
    config = `${config} ph_factory.autotok=${factoryToken}`
  }

  if (wifiConfig && wifiConfig.ssid && wifiConfig.password) {
    config = `${config} pvnet_ssid=${wifiConfig.ssid} pvnet_pass=${wifiConfig.password}`
  }

  if (extraCmdline && extraCmdline.length > 0) {
    config = `${config} ${extraCmdline}`
  }

  return window.btoa(`${config}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0`)
}

export const DownloadImageWithAuth = (options = defaultOptions) => async (_, getState) => {
  const token = getState().auth.token
  try {
    const factoryToken = options.autoClaim
      ? await getFactoryToken(token, options.payload)
      : { json: { token: null } }
    const config = getConfig(factoryToken.json.token, options.wifiConfig, options.cmdline)
    const params = {
      device: options.device,
      devicenick: options.devicenick,
      channel: options.channel,
      releaseindex: options.releaseindex,
      bucket: options.bucket,
      project: options.project,
      variant: options.variant,
      config
    }

    if (options.source.indexOf('https://pantavisor-ci.s3.amazonaws.com') < 0) {
      params.source = options.source
    }

    const downloadURL = BuildURLWithParams(IMAGER_URL, params)
    console.info(downloadURL)
    const popup = window.open(downloadURL, '_black', 'noopener,noreferrer')
    popup.blur()
    window.focus()
  } catch (e) {
    console.info(e)
    return e
  }
}

export const getImages = (channel = 'stable') => async (dispatch, getState) => {
  dispatch(getActions.inProgress())

  return processService(
    async () => {
      const ciResponse = await getImagesData()
      if (!ciResponse.ok) {
        return ciResponse
      }

      const releases = resolvePath(ciResponse, `json.${channel}.0.devices`, [])
        .reduce((acc = [], device = {}) => {
          return acc.concat(resolvePath(device, 'images', []).map((image = {}) => {
            return {
              name: device.platform,
              rev: device.pantahubrevision,
              target: device.target,
              type: image.name,
              checksum: image.checksum,
              url: image.url
            }
          }))
        }, [])
        .filter((release) => !!release.url)

      return {
        ok: ciResponse.ok,
        redirected: ciResponse.redirected,
        headers: ciResponse.headers,
        status: ciResponse.status,
        json: releases
      }
    },
    (resp) => dispatch(getActions.success(resp)),
    (error) => dispatch(catchError(error, getActions.failure))
  )
}
