import { buildBasicReducers } from '../../lib/redux.helpers'
import * as Types from './types'

export const STATES = {
  IMAGES_RELEASES_GET: {
    IN_PROGRESS: 'IMAGES_RELEASES_GET_IN_PROGRESS',
    FAILURE: 'IMAGES_RELEASES_GET_FAILURE',
    SUCCESS: 'IMAGES_RELEASES_GET_SUCCESS'
  }
}

const initialState = {
  items: []
}

const reducer = {
  ...buildBasicReducers(
    STATES.IMAGES_RELEASES_GET,
    Types,
    'IMAGES_RELEASES_GET',
    'items'
  ),
  default: (state, action) => state
}

export const reduce = (state = initialState, action) => {
  const r = reducer[action.type] || reducer.default
  return r(state, action)
}
