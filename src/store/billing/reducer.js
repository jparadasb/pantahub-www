/* eslint-disable no-console */
/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import { buildBasicReducers } from '../../lib/redux.helpers'
// import { resolvePath } from '../../lib/object.helpers'

export const defaultBilling = {
  customer: {},
  payment: null,
  subscriptions: {
    resource: '',
    page_size: 0,
    page_offset: 0,
    current_page: 0,
    total: 0,
    next: '',
    prev: '',
    current: null,
    items: []
  },
  subscription: {},
  prices: {
    resource: '',
    page_size: 0,
    page_offset: 0,
    current_page: 0,
    total: 0,
    next: '',
    prev: '',
    items: []
  }
}

export const STATES = {
  CUSTOMER_GET: {
    IN_PROGRESS: 'CUSTOMER_GET_IN_PROGRESS',
    FAILURE: 'CUSTOMER_GET_FAILURE',
    SUCCESS: 'CUSTOMER_GET_SUCCESS'
  },
  CUSTOMER_UPDATE: {
    IN_PROGRESS: 'CUSTOMER_UPDATE_IN_PROGRESS',
    FAILURE: 'CUSTOMER_UPDATE_FAILURE',
    SUCCESS: 'CUSTOMER_UPDATE_SUCCESS'
  },
  SUBSCRIPTIONS_GET: {
    IN_PROGRESS: 'SUBSCRIPTIONS_GET_IN_PROGRESS',
    FAILURE: 'SUBSCRIPTIONS_GET_FAILURE',
    SUCCESS: 'SUBSCRIPTIONS_GET_SUCCESS'
  },
  SUBSCRIPTION_GET: {
    IN_PROGRESS: 'SUBSCRIPTION_GET_IN_PROGRESS',
    FAILURE: 'SUBSCRIPTION_GET_FAILURE',
    SUCCESS: 'SUBSCRIPTION_GET_SUCCESS'
  },
  SUBSCRIPTION_UPDATE: {
    IN_PROGRESS: 'SUBSCRIPTION_UPDATE_IN_PROGRESS',
    FAILURE: 'SUBSCRIPTION_UPDATE_FAILURE',
    SUCCESS: 'SUBSCRIPTION_UPDATE_SUCCESS'
  },
  PRICES_GET: {
    IN_PROGRESS: 'PRICES_GET_IN_PROGRESS',
    FAILURE: 'PRICES_GET_FAILURE',
    SUCCESS: 'PRICES_GET_SUCCESS'
  },
  PAYMENT_GET: {
    IN_PROGRESS: 'PAYMENT_GET_IN_PROGRESS',
    FAILURE: 'PAYMENT_GET_FAILURE',
    SUCCESS: 'PAYMENT_GET_SUCCESS'
  },
  PAYMENT_UPDATE: {
    IN_PROGRESS: 'PAYMENT_UPDATE_IN_PROGRESS',
    FAILURE: 'PAYMENT_UPDATE_FAILURE',
    SUCCESS: 'PAYMENT_UPDATE_SUCCESS'
  }
}

export const initialState = {
  ...defaultBilling,
  error: undefined,
  status: undefined
}

const reducer = {
  ...buildBasicReducers(
    STATES.CUSTOMER_GET,
    Types,
    'CUSTOMER_GET',
    'customer'
  ),
  ...buildBasicReducers(
    STATES.CUSTOMER_UPDATE,
    Types,
    'CUSTOMER_UPDATE',
    'customer'
  ),
  ...buildBasicReducers(
    STATES.SUBSCRIPTIONS_GET,
    Types,
    'SUBSCRIPTIONS_GET',
    'subscriptions'
  ),
  ...buildBasicReducers(
    STATES.SUBSCRIPTION_GET,
    Types,
    'SUBSCRIPTION_GET',
    'subscription'
  ),
  ...buildBasicReducers(
    STATES.SUBSCRIPTION_UPDATE,
    Types,
    'SUBSCRIPTION_UPDATE',
    'subscription'
  ),
  ...buildBasicReducers(
    STATES.PRICES_GET,
    Types,
    'PRICES_GET',
    'prices'
  ),
  ...buildBasicReducers(
    STATES.PAYMENT_GET,
    Types,
    'PAYMENT_GET',
    'payment'
  ),
  ...buildBasicReducers(
    STATES.PAYMENT_UPDATE,
    Types,
    'PAYMENT_UPDATE',
    'payment'
  ),
  default: (state, action) => state
}

export const reduce = (state = initialState, action) => {
  const r = reducer[action.type] || reducer.default
  return r(state, action)
}
