/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const CUSTOMERS_UPDATE = 'billing/customer/update'
export const CUSTOMERS_UPDATE_INPROGRESS = 'billing/customer/update/inprogress'
export const CUSTOMERS_UPDATE_SUCCESS = 'billing/customer/update/success'
export const CUSTOMERS_UPDATE_FAILURE = 'billing/customer/update/failure'

export const CUSTOMER_GET = 'billing/customer/get'
export const CUSTOMER_GET_INPROGRESS = 'billing/customer/get/inprogress'
export const CUSTOMER_GET_SUCCESS = 'billing/customer/get/success'
export const CUSTOMER_GET_FAILURE = 'billing/customer/get/failure'

export const SUBSCRIPTIONS_GET = 'billing/subscriptions/get'
export const SUBSCRIPTIONS_GET_INPROGRESS = 'billing/subscriptions/get/inprogress'
export const SUBSCRIPTIONS_GET_SUCCESS = 'billing/subscriptions/get/success'
export const SUBSCRIPTIONS_GET_FAILURE = 'billing/subscriptions/get/failure'

export const SUBSCRIPTION_GET = 'billing/subscription/get'
export const SUBSCRIPTION_GET_INPROGRESS = 'billing/subscription/get/inprogress'
export const SUBSCRIPTION_GET_SUCCESS = 'billing/subscription/get/success'
export const SUBSCRIPTION_GET_FAILURE = 'billing/subscription/get/failure'

export const SUBSCRIPTION_UPDATE = 'billing/subscriptions/update'
export const SUBSCRIPTION_UPDATE_INPROGRESS = 'billing/subscriptions/update/inprogress'
export const SUBSCRIPTION_UPDATE_SUCCESS = 'billing/subscriptions/update/success'
export const SUBSCRIPTION_UPDATE_FAILURE = 'billing/subscriptions/update/failure'

export const PRICES_GET = 'billing/prices/get'
export const PRICES_GET_INPROGRESS = 'billing/prices/get/inprogress'
export const PRICES_GET_SUCCESS = 'billing/prices/get/success'
export const PRICES_GET_FAILURE = 'billing/prices/get/failure'

export const PAYMENT_GET = 'billing/payment/get'
export const PAYMENT_GET_INPROGRESS = 'billing/payment/get/inprogress'
export const PAYMENT_GET_SUCCESS = 'billing/payment/get/success'
export const PAYMENT_GET_FAILURE = 'billing/payment/get/failure'

export const PAYMENT_UPDATE = 'billing/payment/update'
export const PAYMENT_UPDATE_INPROGRESS = 'billing/payment/update/inprogress'
export const PAYMENT_UPDATE_SUCCESS = 'billing/payment/update/success'
export const PAYMENT_UPDATE_FAILURE = 'billing/payment/update/failure'
