/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'

import * as Service from '../../services/profiles.service'
import { processService } from '../../services/api.service'
import { buildBasicActions } from '../../lib/redux.helpers'
import { catchError } from '../general-flash/actions'

const getActions = buildBasicActions(Types, 'PROFILE_GET')
const updateActions = buildBasicActions(Types, 'PROFILE_UPDATE')

export const getProfile = (nick) => async (dispatch, getState) => {
  dispatch(getActions.inProgress())
  const state = getState()

  if (!nick) {
    nick = state.auth.nick
  }
  return processService(
    Service.getProfileData.bind(null, nick, state.auth.token),
    (resp) => dispatch(getActions.success(resp)),
    (error) => dispatch(catchError(error, getActions.failure))
  )
}

export const updateApp = (payload) => async (dispatch, getState) => {
  dispatch(updateActions.inProgress())
  const state = getState()

  return processService(
    Service.putProfileData.bind(null, payload, state.auth.token),
    (resp) => dispatch(updateActions.success(resp)),
    (error) => dispatch(catchError(error, updateActions.failure))
  )
}
