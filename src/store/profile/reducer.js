/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import { buildBasicReducers } from '../../lib/redux.helpers'
// import { resolvePath } from '../../lib/object.helpers'

export const DefaultProfile = {
  nick: '',
  fullName: '',
  bio: '',
  picture: '',
  website: '',
  github: '',
  gitlab: '',
  company: '',
  twitter: ''
}

export const STATES = {
  GET_PROFILE: {
    IN_PROGRESS: 'GET_PROFILE_IN_PROGRESS',
    FAILURE: 'GET_PROFILE_FAILURE',
    SUCCESS: 'GET_PROFILE_SUCCESS'
  },
  PUT_PROFILE: {
    IN_PROGRESS: 'PUT_PROFILE_IN_PROGRESS',
    FAILURE: 'PUT_PROFILE_FAILURE',
    SUCCESS: 'PUT_PROFILE_SUCCESS'
  }
}

export const initialState = {
  error: undefined,
  status: undefined,
  current: {
    ...DefaultProfile
  }
}

const reducer = {
  ...buildBasicReducers(
    STATES.GET_PROFILE,
    Types,
    'PROFILE_GET',
    'current'
  ),
  ...buildBasicReducers(
    STATES.PUT_PROFILE,
    Types,
    'PROFILE_UPDATE',
    'current'
  ),
  default: (state, action) => state
}

export const reduce = (state = initialState, action) => {
  const r = reducer[action.type] || reducer.default
  return r(state, action)
}
