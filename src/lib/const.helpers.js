/* eslint-disable camelcase */
/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { resolvePath } from './object.helpers'
import environment from './environment'
import { loadStripe } from '@stripe/stripe-js'

window.env = environment || {}

if (!window.env.REACT_APP_API_URL) {
  window.env.REACT_APP_API_URL = ''
  window.env.REACT_APP_GA_KEY = ''
}

export const getDefaultServiceURL = (service = 'api') => {
  const key = `REACT_APP_${service.toUpperCase()}_URL`
  if (window.env[key]) {
    return window.env[key]
  }

  return window.location.origin.replace('www', service)
}

export const GetEnv = (env, def) => {
  return resolvePath(window, `env.${env}`, def)
}

export const GetApiURL = () => getDefaultServiceURL('api')
export const GetWwwURL = () => getDefaultServiceURL('www')
export const GetPvrURL = () => getDefaultServiceURL('pvr')
export const GetBillingApiUrl = () =>
  GetEnv('REACT_APP_BILLING_API_URL', 'https://stage.billing.pantacor.com/api/v1')

let stripePromise = null
export const LoadStripePromise = () => {
  if (!stripePromise) {
    stripePromise = loadStripe(GetEnv('REACT_APP_STRIPE_PUBLIC_KEY'))
  }

  return stripePromise
}
